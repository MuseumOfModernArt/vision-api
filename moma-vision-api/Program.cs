﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using Google.Cloud.Vision.V1;

namespace moma_vision_api
{
    class Program
    {
        static void Main(string[] args)
        {
            string m_file_path = ConfigurationManager.AppSettings["file_path"].ToString();
            string[] m_files = File.ReadAllLines(ConfigurationManager.AppSettings["file_list"].ToString());

            // Connect to database
            string strConn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(strConn);
            conn.Open();

            SqlCommand m_cmd = new SqlCommand("procMomaVisionApi", conn);
            m_cmd.CommandType = CommandType.StoredProcedure;

            // Check each file
            for (int i = 0; i < m_files.Length; i++)
            {

                // Instantiates a client
                var client = ImageAnnotatorClient.Create();
                // Load the image file into memory
                var image = Image.FromFile(m_file_path + m_files[i].ToString());
                // Performs label detection on the image file
                var response = client.DetectLabels(image);

                foreach (var annotation in response)
                {
                    if (annotation.Description != null)

                    m_cmd.Parameters.Clear();

                    m_cmd.Parameters.Add(new SqlParameter(@"@p_file_name", SqlDbType.VarChar) { Value = m_files[i].ToString() });
                    m_cmd.Parameters.Add(new SqlParameter(@"@p_tag", SqlDbType.VarChar) { Value = annotation.Description });
                    m_cmd.Parameters.Add(new SqlParameter(@"@p_score", SqlDbType.Float) { Value = annotation.Score });

                    m_cmd.ExecuteNonQuery();

                    Console.WriteLine(m_files[i].ToString());
                    Console.WriteLine(annotation.Description);
                }

            }

        }
    }
}
