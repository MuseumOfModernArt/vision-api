# vision-api

### Batch process multiple images using Google's Vision API.  You provide a list of files in a text file and the program iterates through the list and uses the vision api to get results.  The json of the api is then parsed and inserted into a database.  ###


Note: this code uses a single database table and one stored procedure.  the ddl to create both in mssql is below:

    CREATE TABLE [dbo].[tags](
	    [id] [int] IDENTITY(1,1) NOT NULL,
	    [object_id] [int] NULL,
	    [object_number] [nvarchar](50) NULL,
	    [file_name] [nvarchar](255) NULL,
	    [tag] [nvarchar](255) NULL,
	    [score] [float] NULL,
     CONSTRAINT [PK_tags1] PRIMARY KEY CLUSTERED 
    (
	    [id] ASC
    ) ON [PRIMARY]
    ) ON [PRIMARY]

    GO
	
	CREATE PROCEDURE [dbo].[procMomaVisionApi] 
		
		@p_file_name nvarchar(255),
		@p_tag nvarchar(255),
		@p_score float
	AS
	BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;
		
		INSERT INTO [dbo].[tags] ([file_name],[tag],[score])
		 VALUES
			(
			   @p_file_name,
			   @p_tag,
			   @p_score
			)
	END	
	
### I also did a ui to view the data with the images.  Below is before and after hover ###

-- Before

![before](before.png)

-- After

![after](after.png)